require 'pomodoro'

describe 'Pomodoro' do
  let (:default_duration) { 25 }

  it 'cannot be created with null values' do
    expect {
      Pomodoro.create(nil)
    }.to raise_error('Cannot be created with null values')
  end

  context 'is created' do
    it 'with a duration by default' do

      timeleft = Pomodoro.create.timeleft

      expect(timeleft).to eq(default_duration)
    end

    it 'with a specific duration' do
      specific_duration = 10000

      pomodoro = Pomodoro.create(specific_duration)

      expect(pomodoro.timeleft).to eq(specific_duration)
    end

    it 'with negative durations has a default duration' do
      any_negative_duration = -123

      pomodoro = Pomodoro.create(any_negative_duration)

      expect(pomodoro.timeleft).to eq(default_duration)
    end

    it 'without interruptions' do
      pomodoro = Pomodoro.create

      interruptions = pomodoro.interruptions

      expect(interruptions).to be_zero
    end
  end

  it 'can be started' do
    pomodoro = Pomodoro.create

    pomodoro.start

    expect(pomodoro.timeleft).to eq(24)
  end

  it 'is done when there are not time left' do
    pomodoro = Pomodoro.create
    pomodoro.start

    pass_a_lot_of_time_for(pomodoro)

    expect(pomodoro.timeleft).to be_zero
  end

  context 'has interruptions' do
    it 'can not count it when is not started' do
      pomodoro = Pomodoro.create

      pomodoro.count_interruption

      expect(pomodoro.interruptions).to be_zero
    end

    it 'are counted when is started' do
      pomodoro = Pomodoro.create
      pomodoro.start

      pomodoro.count_interruption
      pomodoro.count_interruption

      expect(pomodoro.interruptions).to eq(2)
    end
  end

  it 'restarts when use start twice' do
    pomodoro = Pomodoro.create(11)
    pomodoro.start
    pass_a_lot_of_time_for(pomodoro)

    pomodoro.start

    expect(pomodoro.timeleft).to eq(10)
  end

  it 'resets interruptions when restarts' do
    pomodoro = Pomodoro.create
    pomodoro.start
    pomodoro.count_interruption

    pomodoro.start

    expect(pomodoro.interruptions).to be_zero
  end

  def pass_a_lot_of_time_for(pomodoro)
    76854.times do
      pomodoro.timeleft
    end
  end
end
