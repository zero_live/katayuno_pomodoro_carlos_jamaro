
class Duration
  CREATION_ERROR = 'Cannot be created with null values'
  DEFAULT = 25

  def initialize(value)
    raise CREATION_ERROR if value.nil?

    @value = normalize(value)
    @initial = @value
  end

  def restart
    @value = @initial
  end

  def pass_time
    @value -= 1 unless zero?
  end

  def current
    @value
  end

  private

  def normalize(value)
    return DEFAULT if negative?(value)

    value
  end

  def negative?(value)
    value < 0
  end

  def zero?
    @value.zero?
  end
end
