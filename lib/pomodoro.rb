require 'pomodoro/duration'

class Pomodoro
  INITIAL_INTERRUPTIONS = 0

  def self.create(duration=Duration::DEFAULT)
    new(duration)
  end

  def initialize(duration)
    @duration = Duration.new(duration)
    @new_state = State.new

    @interruptions = INITIAL_INTERRUPTIONS
    @state = nil
  end

  def start
    restart if started?

    @state = :started
  end

  def count_interruption
    @interruptions += 1 if started?
  end

  def timeleft
    calculate_time_passed if started?

    @duration.current
  end

  def interruptions
    @interruptions
  end

  private

  def calculate_time_passed
    @duration.pass_time
  end

  def restart
    @interruptions = INITIAL_INTERRUPTIONS
    @duration.restart
  end

  def started?
    @state == :started
  end
end
